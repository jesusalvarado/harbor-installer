#!/bin/bash
# Taken from the original common.sh file insode HArbor installer

set +e
set -o noglob

#
# Set Colors
#

bold=$(tput bold)
underline=$(tput sgr 0 1)
reset=$(tput sgr0)

red=$(tput setaf 1)
green=$(tput setaf 76)
white=$(tput setaf 7)
tan=$(tput setaf 202)
blue=$(tput setaf 25)
orange=$(tput setaf 208)

#
# Headers and Logging
#

underline() { printf "${underline}${bold}%s${reset}\n" "$@"
}
h1() { printf "\n${underline}${bold}${blue}%s${reset}\n\n" "$@"
}
h2() { printf "\n${underline}${bold}${orange}%s${reset}\n\n" "$@"
}
h3() { printf "\n ${underline}${bold}${orange}%s${reset}\n\n" "$@"
}
debug() { printf "${white}%s${reset}\n" "$@"
}
info() { printf "${white}➜ %s${reset}\n" "$@"
}
success() { printf "${green}✔ %s${reset}\n" "$@"
}
error() { printf "${red}✖ %s${reset}\n" "$@"
}
warn() { printf "${tan}➜ %s${reset}\n" "$@"
}
bold() { printf "${bold}%s${reset}\n" "$@"
}
note() { printf "\n${underline}${bold}${blue}Note:${reset} ${blue}%s${reset}\n" "$@"
}

set -e

function check_golang { 
	local result=1
	if [ -z $(command -v go) ]
	then
		result=0
	else
		# docker has been installed and check its version
		if [[ $(GOROOT=~/go && go version) =~ (([0-9]+)\.([0-9]+)([\.0-9]*)) ]]
		then
			golang_version=${BASH_REMATCH[1]}
			golang_version_part1=${BASH_REMATCH[2]}
			golang_version_part2=${BASH_REMATCH[3]}

			# the version of golang does not meet the requirement
			if [ "$golang_version_part1" -lt 1 ] || ([ "$golang_version_part1" -eq 1 ] && [ "$golang_version_part2" -lt 12 ])
			then
				result=0
			fi 
		fi
	fi

	
	echo $result
}

function check_docker {
	local result=1
	if [ -z $(command -v docker) ]
	then
		result=0
	else
		# docker has been installed and check its version
		if [[ $(docker --version) =~ (([0-9]+)\.([0-9]+)([\.0-9]*)) ]]
		then
			docker_version=${BASH_REMATCH[1]}
			docker_version_part1=${BASH_REMATCH[2]}
			docker_version_part2=${BASH_REMATCH[3]}

			# the version of docker does not meet the requirement
			if [ "$docker_version_part1" -lt 17 ] || ([ "$docker_version_part1" -eq 17 ] && [ "$docker_version_part2" -lt 6 ])
			then
				result=0
			fi
		fi
	fi
	echo $result
}

function check_dockercompose {
	result=1
	if [ -z $(command -v docker-compose) ]
	then
		result=0
	else
		if [[ $(docker-compose version) =~ (([0-9]+)\.([0-9]+)([\.0-9]*)) ]]
		then
			docker_compose_version=${BASH_REMATCH[1]}
			docker_compose_version_part1=${BASH_REMATCH[2]}
			docker_compose_version_part2=${BASH_REMATCH[3]}

			# the version of docker-compose does not meet the requirement
			if [ "$docker_compose_version_part1" -lt 1 ] || ([ "$docker_compose_version_part1" -eq 1 ] && [ "$docker_compose_version_part2" -lt 18 ])
			then
				result=0
			fi
		fi
	fi

	# docker-compose has been installed, check its version
	
	echo $result
}