#!/bin/bash
step=1
set -e

DIR="$(cd "$(dirname "$0")" && pwd)"
source $DIR/common.sh
source $DIR/config.sh

function generate_cert() {
  h1 "Generating certificates"

  ## Authority Certificate
  h2 "[Step $@ - $step]: Creating Authority certificate"; let step+=1
  openssl genrsa -out ca.key 4096
  openssl req -x509 -new -nodes -sha512 -days 3650 \
  -subj "/C=MX/ST=Mexico/L=Mexico\ City/CN=$(hostname)" \
  -key ca.key \
  -out ca.crt
  if [ -f ca.key ] && [ -f ca.crt ]
  then
    success "Authority certificate generated"
  else
    error "Authority certificate not generated, Verify manually"
  fi

  # Server Certificate
  h2 "[Step $@ - $step]: Creating server certificate"; let step+=1
  openssl genrsa -out $(hostname).key 4096
  openssl req -sha512 -new \
  -subj "/C=MX/ST=Mexico/L=Mexico\ City/CN=$(hostname)" \
  -key $(hostname).key \
  -out $(hostname).csr
  if [ -f $(hostname).crt ] && [ -f $(hostname).key ]
  then
    success "Server certificate generated"
  else
    error "Server certificate not generated, Verify manually"
  fi

  ## Generate x509v3 extension file
  h2 "[Step $@ - $step]: Generating x509v3 extention file"; let step+=1
  cat > v3.ext <<-EOF
  authorityKeyIdentifier=keyid,issuer
  basicConstraints=CA:FALSE
  keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
  extendedKeyUsage = serverAuth
  subjectAltName = @alt_names
  [alt_names]
  DNS.1=$(hostname)
  DNS.2=bolillodigital
  DNS.3=harbor
EOF
  openssl x509 -req -sha512 -days 3650 \
    -extfile v3.ext \
    -CA ca.crt -CAkey ca.key -CAcreateserial \
    -in $(hostname).csr \
    -out $(hostname).crt
  if [ -f v3.ext ]
  then
    success "v3.ext file generated"
  else
    error "Verify manually"
  fi


  ## Copy certificate
  h2 "[Step $@ - $step]: Copiying certificate"; let step+=1
  mkdir -p /data/cert
  cp $(hostname).crt /data/cert/
  cp $(hostname).key /data/cert/
  if [ -f /data/cert/$(hostname).crt ] && [ -f /data/cert/$(hostname).key ]
  then
    success "Certificates copied to /data/cert"
  else
    error "Verify manually"
  fi

  ## Convert server certificate
  h2 "[Step $@ - $step]: Converting certificate"; let step+=1
  openssl x509 -inform PEM -in $(hostname).crt -out $(hostname).cert
  if [ -f $(hostname).cert ] 
  then
    success "Server certificate converted ( $(hostname).cert )"
  else
    error "Verify manually"
  fi

  ## Copy certificates to Docker certificates folder
  h2 "[Step $@ - $step]: Copiying to docker folder"; let step+=1
  mkdir -p /etc/docker/certs.d/$(hostname)/
  cp $(hostname).cert /etc/docker/certs.d/$(hostname)/
  cp $(hostname).key /etc/docker/certs.d/$(hostname)/
  cp ca.crt /etc/docker/certs.d/$(hostname)/
  if [ -f /etc/docker/certs.d/$(hostname)/$(hostname).cert ] && [ -f /etc/docker/certs.d/$(hostname)/$(hostname).key ]
  then
    success "Certificates copied to /etc/docker/certs.d/$(hostname)/"
  else
    error "Verify manually"
  fi
  warn "Restarting Docker service"
  systemctl restart docker
  
}