#!/bin/bash

step=1
set -e

GO="go1.18.3.linux-amd64.tar.gz"
DIR="$(cd "$(dirname "$0")" && pwd)"
source $DIR/common.sh
source $DIR/config.sh
source $DIR/cert.sh

function install_docker() {
    h3 "Installing Docker..."
    dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
    dnf install docker-ce --allowerasing -y
    systemctl enable --now docker
    usermod -aG docker $USER
    sed -i 's/FirewallBackend=nftables/FirewallBackend=iptables/g' /etc/firewalld/firewalld.conf
    sudo systemctl restart firewalld.service
    if  command -v docker --version
    then

        success "Docker installed, $(docker --version)"
    fi
}

function install_docker_compose() {
    h3 "Installing Docker Compose..."
    dnf -y install wget docker-compose-plugin.x86_64
    curl -q -L https://github.com/docker/compose/releases/download/v2.6.0/docker-compose-linux-x86_64 -o /usr/bin/docker-compose
    chmod +x /usr/bin/docker-compose
    if  command -v docker-compose version 
    then

        success "Docker Compose installed, $(docker-compose version)"
    fi
}

function install_golang() {
    cd $DIR
    h3 "Installing GO..."
    wget -q -L https://go.dev/dl/$GO 
    rm -rf /usr/go && tar -xzf $GO
    cp -r go/bin/ /usr/
    rm -rf $GO
    mkdir -p ~/go
    if  [ ! -z $(command -v go version) ]
    then
        success "GO installed, $(export GOROOT=~/go && go version )"
    fi
}

function install_harbor() {
    generate_cert "$step"
    cd $INSTALL_DIR
    h3 "Installing Harbor $HARBOR_VER on $INSTALL_DIR ..."
    wget -L https://github.com/goharbor/harbor/releases/download/$HARBOR_VER/harbor-offline-installer-$HARBOR_VER.tgz 
    tar -xvf harbor-offline-installer-$HARBOR_VER.tgz
    rm harbor-offline-installer-$HARBOR_VER.tgz 
    cd harbor
    if [ -f $DIR/harbor.yml ]
    then
        info "Copiying provided Harbor.yml file"
        cp $DIR/harbor.yml .
    else
        warn "Using default Harbor.yml.tmpl"
        cp harbor.yml.tmpl harbor.yml
    fi
    
    sed -i 's/hostname: reg.mydomain.com/hostname: $(hostname)/g' harbor.yml
    sed -i "s|certificate: /your/certificate/path|certificate: /data/cert/$(hostname).crt|g" harbor.yml
    sed -i "s|private_key: /your/private/key/path|private_key: /data/cert/$(hostname).key|g" harbor.yml
    sed -i "s/harbor_admin_password: Harbor12345/harbor_admin_password: ${HARBOR_PASSWORD}/g" harbor.yml
    sed -i "s/password: root123/password: ${DB_PASSWORD}/g" harbor.yml
    
    note "Installing with $SCANNER"
    if [[ "$SCANNER" == "notary" ]]
    then
        ./install.sh --with-notary --with-chartmuseum
    else
        ./install.sh --with-trivy --with-chartmuseum
    fi
}

h1 "Docker Harbor Installer"
info "User: $USER"

h2 "[Step $step]: Installing dependencies"; let step+=1
if [ -z $(command -v wget) ] || [ -z $(command -v curl) ]
then
    dnf update -y
    dnf install.sh -y wget curl
fi

h2 "[Step $step]: Checking Docker installation"; let step+=1
has_docker=$(check_docker)
if [[ "$has_docker" == "0" ]]
then
    install_docker
else
    success "Docker is installed, $(docker --version)"
fi



h2 "[Step $step]: Checking Docker Compose installation"; let step+=1
has_dockercompose=$(check_dockercompose)
if [[ "$has_dockercompose" == "0" ]]; then
    install_docker_compose
else    
    success "Docker Compose is installed, $(docker compose version)"
fi


h2 "[Step $step]: Checking Go installation"; let step+=1
has_go=$(check_golang)
if [[ "$has_go" == "0" ]]
then
    install_golang
else
    success "GO is installed, $(export GOROOT=~/go && go version )"
fi


h2 "[Step $step]: Getting Harbor official installer"; let step+=1
install_harbor




